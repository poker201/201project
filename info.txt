Poker Island ReadMe
	Poker Island is a online free-to-play Poker application created as an educational endeavor for USC's CSCI 201 class.

Contributors
	Utkash Dubey udubey@usc.edu 30393
	Emile Indik indik@usc.edu 30381
	Adam Mann adammann@usc.edu 30381
	Curren Mehta currenme@usc.edu 30381
	Noel Trivedi nttrived@usc.edu 30381

Database Login Information
	ID: poker
	username: poker
	pass: poker201

Database Hosting
	Hosted via amazon aws
	Account name: NUCHE201@gmail.com
	Password: poker201

Exporting Files from Eclipse
	Go to file>export
	In the Java folder, click on Runnable JAR File
	Press Next
	Choose the Launch configuration (either "Client - 201 project" or "Server - 201 project")
	In Library handling, select the one that starts with "Package..."
	Uncheck "Save as ANT script"
	Press Finish